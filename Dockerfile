FROM openjdk:11
EXPOSE 8761
ADD build/libs/*.jar discovery.jar
ENV JAVA_OPTS = "-Xmx512m"
ENTRYPOINT ["sh","-c", "java -Xmx1024m -jar discovery.jar"]